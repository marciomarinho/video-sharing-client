import React, { useEffect } from "react";
import VideoPlayer from "../components/VideoPlayer"
import "video.js/dist/video-js.css"

import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";

export default class SharedVideo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isShow: true,
    };
   
    this.state = {
      src: this.props.location.state.videoUrl,
      poster: this.props.location.state.posterUrl,
      title: this.props.location.state.title,
      description: this.props.location.state.description
    }
  }

  

  render() {

    console.log(this.state);

    return (
      <div className="container">
        <FormGroup controlId="title">
          <ControlLabel>Title</ControlLabel>
          <FormControl
            value={this.state.title}
            type="text"
            disabled
          />
        </FormGroup>
        <VideoPlayer  {...  {
          autoplay: true,
          controls: true,
          sources: [{
            src: this.props.location.state.src,
            poster: this.props.location.state.posterUrl,
            type: 'application/x-mpegURL'
          }]
        }} />
        <FormGroup controlId="description">
          <ControlLabel>Description</ControlLabel>
          <FormControl
            value={this.state.description}
            componentClass="textarea"
            disabled
          />
        </FormGroup>
      </div>
    )
  }

}
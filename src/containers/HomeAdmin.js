import React, { useState, useEffect } from "react";
import { API } from "aws-amplify";
import { LinkContainer } from "react-router-bootstrap";
import { PageHeader, ListGroup, ListGroupItem } from "react-bootstrap";
import "./Home.css";

export default function HomeAdmin(props) {
  const [videos, setvideos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function onLoad() {
      if (!props.isAuthenticated) {
        return;
      }

      try {
        const videos = await loadvideos();
        setvideos(videos);
      } catch (e) {
        alert(e);
      }

      setIsLoading(false);
    }

    onLoad();
  }, [props.isAuthenticated]);

  function loadvideos() {
    return API.get("videos", "/videos");
  }

  function rendervideosList(videos) {
    return [{}].concat(videos).map((video, i) =>
      i !== 0 ? (
        <LinkContainer key={video.id} to={`/videos/${video.id}`}>
          <ListGroupItem header={video.title.trim().split("\n")[0]}>
            {"Created: " + new Date(video.createdAt).toLocaleString()}
          </ListGroupItem>
        </LinkContainer>
      ) : (
        <LinkContainer key="new" to="/videos/new">
          <ListGroupItem>
            <h4>
              <b>{"\uFF0B"}</b> Create a new note
            </h4>
          </ListGroupItem>
        </LinkContainer>
      )
    );
  }

  function renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple note taking app</p>
      </div>
    );
  }

  function rendervideos() {
    return (
      <div className="container">
        <PageHeader>Your videos</PageHeader>
        <ListGroup>
          {!isLoading && rendervideosList(videos)}
        </ListGroup>
      </div>
    );
  }

  return (
    <div className="Home">
      {props.isAuthenticated ? rendervideos() : renderLander()}
    </div>
  );
}

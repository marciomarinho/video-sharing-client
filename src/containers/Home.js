import React, { useState, useEffect } from "react";
import { API } from "aws-amplify";
import { LinkContainer } from "react-router-bootstrap";
import { PageHeader, ListGroup, ListGroupItem } from "react-bootstrap";
import "./Home.css";

export default function Home(props) {
  const [videos, setvideos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function onLoad() {
      if (!props.isAuthenticated) {
        return;
      }

      try {
        const videos = await loadVideos();
        setvideos(videos);
      } catch (e) {
        alert(e);
      }

      setIsLoading(false);
    }

    onLoad();
  }, [props.isAuthenticated]);

  function loadVideos() {
    return API.get("videos", "/videos");
  }

  function renderVideosList(videos) {
    return [{}].concat(videos).map((video, i) =>
      i !== 0 ? (
        <LinkContainer key={video.id} to={{
          pathname: `/show/${video.id}`,
          state: {
            src: video.videoUrl,
            poster: video.posterUrl,
            title: video.title,
            description: video.description
          }
          }}>
          <ListGroupItem header={video.title.trim().split("\n")[0]}>
            {"Created: " + new Date(video.createdAt).toLocaleString()}
          </ListGroupItem>
        </LinkContainer>
      ) : (
        <LinkContainer key="new" to="">
          <ListGroupItem>
            <h4>
              <b>{"\uFF0B"}</b> All videos
            </h4>
          </ListGroupItem>
        </LinkContainer>
      )
    );
  }

  function renderLander() {
    return (
      <div className="lander">
        <h1>Shareable</h1>
        <p>A simple video sharing app</p>
      </div>
    );
  }

  function renderVideos() {
    return (
      <div className="container">
        <PageHeader>Your videos</PageHeader>
        <ListGroup>
          {!isLoading && renderVideosList(videos)}
        </ListGroup>
      </div>
    );
  }

  return (
    <div className="Home">
      {props.isAuthenticated ? renderVideos() : renderLander()}
    </div>
  );
}

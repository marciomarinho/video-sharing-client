import React, { useRef, useState } from "react";
import { API } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { s3Upload } from "../libs/awsLib";
import config from "../config";
import "./NewVideo.css";

export default function NewVideo(props) {
  const file = useRef(null);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const uuidv1 = require('uuid/v1');

  function validateForm() {
    return title.length > 0 && description.length > 0;
  }

  function handleFileChange(event) {
    file.current = event.target.files[0];
  }

  async function handleSubmit(event) {
    event.preventDefault();

    // if (file.current && file.current.size > config.MAX_ATTACHMENT_SIZE) {
    //   alert(
    //     `Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE /
    //       1000000} MB.`
    //   );
    //   return;
    // }

    setIsLoading(true);

    try {

      const newVideo = await createVideo({title, description});

      const metadata = {'video-id': newVideo.id};

      const attachment = file.current
        ? await s3Upload(file.current, metadata)
        : null;

      await saveVideo({
        title,
        description,
        attachment: attachment
      }, newVideo);

      props.history.push("/");
    } catch (e) {
      alert(e);
      setIsLoading(false);
    }
  }

  function createVideo(video) {
    return API.post("videos", "/videos", {
      body: video
    });
  }

  function saveVideo(video, newVideo) {
    return API.patch("videos", `/videos/${newVideo.id}`, {
      body: video
    });
  }

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="title">
          <FormControl
            value={title}
            type="text"
            onChange={e => setTitle(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="description">
          <FormControl
            value={description}
            componentClass="textarea"
            onChange={e => setDescription(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="file">
          <ControlLabel>Attachment</ControlLabel>
          <FormControl onChange={handleFileChange} type="file" />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          bsStyle="primary"
          isLoading={isLoading}
          disabled={!validateForm()}
        >
          Create
        </LoaderButton>
      </form>
    </div>
  );
}

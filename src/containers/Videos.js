import React, { useRef, useState, useEffect } from "react";
import { API, Storage } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { s3Upload } from "../libs/awsLib";
import config from "../config";
import "./Videos.css";

export default function Notes(props) {
  const file = useRef(null);
  const [video, setVideo] = useState(null);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  useEffect(() => {
    function loadVideo() {
      return API.get("videos", `/videos/${props.match.params.id}`);
    }

    async function onLoad() {
      try {
        const video = await loadVideo();
        const { title, description, attachment } = video;

        if (attachment) {
          video.attachmentURL = await Storage.vault.get(attachment);
        }

        setTitle(title);
        setDescription(description);
        setVideo(video);
      } catch (e) {
        alert(e);
      }
    }

    onLoad();
  }, [props.match.params.id]);

  function validateForm() {
    return title.length > 0 && description.length > 0;
  }

  function formatFilename(str) {
    return str.replace(/^\w+-/, "");
  }

  function handleFileChange(event) {
    file.current = event.target.files[0];
  }

  function saveVideo(video) {
    return API.patch("videos", `/videos/${props.match.params.id}`, {
      body: video
    });
  }

  async function handleSubmit(event) {
    let attachment;

    event.preventDefault();

    if (file.current && file.current.size > config.MAX_ATTACHMENT_SIZE) {
      alert(
        `Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE /
          1000000} MB.`
      );
      return;
    }

    setIsLoading(true);

    try {

      if (file.current) {
        let metadata = {'video-id': video_entry.id};
        attachment = await s3Upload(file.current, metadata);
      }
      
      console.log('***** this is the attachment *****');
      
      console.log(attachment);

      let video_entry = await saveVideo({
        title,
        description,
        attachment: attachment || video.attachment
      });

      console.log(video_entry);

      props.history.push("/");
    } catch (e) {
      alert(e);
      setIsLoading(false);
    }
  }

  function deleteVideo() {
    return API.del("videos", `/videos/${props.match.params.id}`);
  }

  async function handleDelete(event) {
    event.preventDefault();

    const confirmed = window.confirm(
      "Are you sure you want to delete this video?"
    );

    if (!confirmed) {
      return;
    }

    setIsDeleting(true);

    try {
      await deleteVideo();
      props.history.push("/");
    } catch (e) {
      alert(e);
      setIsDeleting(false);
    }
  }

  return (
    <div className="container">
      {video && (
        <form onSubmit={handleSubmit}>
          <FormGroup controlId="title">
            <FormControl
              value={title}
              type="text"
              onChange={e => setTitle(e.target.value)}
            />
          </FormGroup>
          <FormGroup controlId="description">
            <FormControl
              value={description}
              componentClass="textarea"
              onChange={e => setDescription(e.target.value)}
            />
          </FormGroup>
          {video.attachment && (
            <FormGroup>
              <ControlLabel>Attachment</ControlLabel>
              <FormControl.Static>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={video.attachmentURL}
                >
                  {formatFilename(video.attachment)}
                </a>
              </FormControl.Static>
            </FormGroup>
          )}
          <FormGroup controlId="file">
            {!video.attachment && <ControlLabel>Attachment</ControlLabel>}
            <FormControl onChange={handleFileChange} type="file" />
          </FormGroup>
          <LoaderButton
            block
            type="submit"
            bsSize="large"
            bsStyle="primary"
            isLoading={isLoading}
            disabled={!validateForm()}
          >
            Save
          </LoaderButton>
          <LoaderButton
            block
            bsSize="large"
            bsStyle="danger"
            onClick={handleDelete}
            isLoading={isDeleting}
          >
            Delete
          </LoaderButton>
        </form>
      )}
    </div>
  );
}

import { Storage } from "aws-amplify";

export async function s3Upload(file, my_metadata) {
  
  const filename = `${Date.now()}-${file.name}`;

  const stored = await Storage.vault.put(filename, file, {
    contentType: file.type,
    metadata: my_metadata
  });

  return stored.key;
}

import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";

import Home from "./containers/Home";
import HomeAdmin from "./containers/HomeAdmin";
import Login from "./containers/Login";
import Videos from "./containers/Videos";
import Signup from "./containers/Signup";
import NewVideo from "./containers/NewVideo";
import NotFound from "./containers/NotFound";
import SharedVideo from "./containers/SharedVideo"

export default function Routes({ appProps }) {
  return (
    <Switch>
      <AppliedRoute path="/" exact component={Home} appProps={appProps} />
      <AppliedRoute path="/show/:id" exact component={SharedVideo} appProps={appProps} />
      <UnauthenticatedRoute path="/login" exact component={Login} appProps={appProps} />
      <UnauthenticatedRoute path="/signup" exact component={Signup} appProps={appProps} />
      <AppliedRoute path="/admin" exact component={HomeAdmin} appProps={appProps} />
      <AuthenticatedRoute path="/admin/videos/new" exact component={NewVideo} appProps={appProps} />
      <AuthenticatedRoute path="/admin/videos/:id" exact component={Videos} appProps={appProps} />
      <Route component={NotFound} />
    </Switch>
  );
}
